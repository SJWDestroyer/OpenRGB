/*-----------------------------------------*\
|  RGBController_AsusAuraGPU.h              |
|                                           |
|  Generic RGB Interface for Asus Aura GPU  |
|                                           |
|  Jan Rettig (Klapstuhl) 14.02.2020        |
\*-----------------------------------------*/

#include "RGBController_AsusAuraGPUv2.h"

int RGBController_AuraGPUv2::GetDeviceMode()
{




    return 1;
}

RGBController_AuraGPUv2::RGBController_AuraGPUv2(AuraGPUControllerv2 * aura_gpu_ptr)
{
    aura_gpu = aura_gpu_ptr;


    name        = aura_gpu->GetDeviceName();
    vendor      = "ASUS";
    type        = DEVICE_TYPE_GPU;
    description = "ASUS Aura GPU Device";
    version     = "0.00.1";
    location    = aura_gpu->GetDeviceLocation();

    mode Direct;
    Direct.name       = "Direct";
    Direct.value      = 2;
    Direct.flags      = MODE_FLAG_HAS_PER_LED_COLOR;
    Direct.color_mode = MODE_COLORS_PER_LED;
    modes.push_back(Direct);

    mode Off;
    Off.name       = "Off";
    Off.value      = 0;
    Off.flags      = 0;
    Off.color_mode = MODE_COLORS_NONE;
    modes.push_back(Off);

    mode Static;
    Static.name       = "Static";
    Static.value      = 1;
    Static.flags      = MODE_FLAG_HAS_PER_LED_COLOR;
    Static.color_mode = MODE_COLORS_PER_LED;
    modes.push_back(Static);

    /*mode Breathing;
    Breathing.name       = "Breathing";
    Breathing.value      = 3;
    Breathing.flags      = MODE_FLAG_HAS_PER_LED_COLOR;
    Breathing.color_mode = MODE_COLORS_PER_LED;
    modes.push_back(Breathing);

    mode Flashing;
    Flashing.name       = "Flashing";
    Flashing.value      = 4;
    Flashing.flags      = MODE_FLAG_HAS_PER_LED_COLOR;
    Flashing.color_mode = MODE_COLORS_PER_LED;
    modes.push_back(Flashing);

    mode Spectrum_Cycle;
    Spectrum_Cycle.name       = "Spectrum Cycle";
    Spectrum_Cycle.value      = 5;
    Spectrum_Cycle.flags      = 0;
    Spectrum_Cycle.color_mode = MODE_COLORS_NONE;
    modes.push_back(Spectrum_Cycle);*/

    SetupZones();

    active_mode = GetDeviceMode();
}

RGBController_AuraGPUv2::~RGBController_AuraGPUv2()
{
    delete aura_gpu;
}

void RGBController_AuraGPUv2::SetupZones()




//    std::string             name;           /* Zone name                */
//    zone_type               type;           /* Zone type                */
//    led *                   leds;           /* List of LEDs in zone     */
//    RGBColor *              colors;         /* Colors of LEDs in zone   */
//    unsigned int            start_idx;      /* Start index of led/color */
//    unsigned int            leds_count;     /* Number of LEDs in zone   */
//    unsigned int            leds_min;       /* Minimum number of LEDs   */
//    unsigned int            leds_max;       /* Maximum number of LEDs   */
//    matrix_map_type *       matrix_map;     /* Matrix map pointer       */


{
    /*---------------------------------------------------------*\
    | Set up zone                                               |
    \*---------------------------------------------------------*/
    zone aura_gpu_zone;


    aura_gpu_zone.name          = "GPU";
    aura_gpu_zone.type          = ZONE_TYPE_LINEAR;
    aura_gpu_zone.leds_min      = 30;
    aura_gpu_zone.leds_max      = 30;
    aura_gpu_zone.leds_count    = 30;
    aura_gpu_zone.matrix_map    = NULL;




    for (int i =0 ; i<aura_gpu_zone.leds_count; i++){
        led gpuLED;
        gpuLED.name = "GPU" + std::to_string(i);
        gpuLED.value = i;
        leds.push_back(gpuLED);
    }

    aura_gpu_zone.leds = leds.data();


    zones.push_back(aura_gpu_zone);


    SetupColors();

    /*---------------------------------------------------------*\
    | Initialize color                                          |
    \*---------------------------------------------------------*/

    //colors[0] =  ToRGBColor(red, grn, blu);
}

void RGBController_AuraGPUv2::ResizeZone(int /*zone*/, int /*new_size*/)
{
    /*---------------------------------------------------------*\
    | This device does not support resizing zones               |
    \*---------------------------------------------------------*/
}

void RGBController_AuraGPUv2::DeviceUpdateLEDs()
{
    std::size_t tmpsize = 0x1F*3;
    unsigned char * temp = new unsigned char[tmpsize];
    std::size_t led = -1;
    int counter = 0;
    while( ++led < colors.size())
    {
        unsigned char rgb [3] ;
        rgb[0] = RGBGetRValue(colors[led]);
        rgb[1] = RGBGetBValue(colors[led]);
        rgb[2] = RGBGetGValue(colors[led]);


        for (int i = 0; i < 3; i++)
        {
            int sizeSpot = ((counter % 0x1F) == 0);
            temp[(counter += sizeSpot)] = rgb[i];
            if (sizeSpot > 0) {
                temp[counter-1] = 0x1E;
            }
                        counter++;
        }





    }

    if (GetMode() == 0)
    {
        aura_gpu->SetLEDColorsDirect(0,0,0, temp);
    }
    else
    {
        aura_gpu->SetLEDColorsEffect(0,0,0, temp);
    }

    delete[] temp;
}

void RGBController_AuraGPUv2::UpdateZoneLEDs(int /*zone*/)
{
    DeviceUpdateLEDs();
}

void RGBController_AuraGPUv2::UpdateSingleLED(int /*led*/)
{
    DeviceUpdateLEDs();
}

void RGBController_AuraGPUv2::SetCustomMode()
{
    active_mode = 0;
}

void RGBController_AuraGPUv2::DeviceUpdateMode()
{
    int new_mode = modes[active_mode].value;
    aura_gpu->direct = false;
     std::size_t tmpsize = 0x1F*3;
    switch(new_mode)
    {

    // Set all LEDs to 0 and Mode to static as a workaround for the non existing Off Mode
    case 0:

        for (int i = 0; i < colors.size(); i++) colors[i] = 0LL;
        new_mode = 1;
        break;

    // Direct mode is done by switching to Static and not applying color changes
    case 2:
        aura_gpu->direct = true;
        new_mode = 1;
        break;
    }

    aura_gpu->SetMode(new_mode);
}
