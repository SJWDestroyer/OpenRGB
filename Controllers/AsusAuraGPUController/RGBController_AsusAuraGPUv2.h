/*-----------------------------------------*\
|  RGBController_AsusAuraGPU.h              |
|                                           |
|  Generic RGB Interface for Asus Aura GPU  |
|                                           |
|  Jan Rettig (Klapstuhl) 14.02.2020        |
\*-----------------------------------------*/

#pragma once

#include "RGBController.h"
#include "AsusAuraGPUControllerv2.h"

class RGBController_AuraGPUv2 : public RGBController
{
public:
    RGBController_AuraGPUv2(AuraGPUControllerv2* aura_gpu_ptr);
    ~RGBController_AuraGPUv2();

    void        SetupZones();

    void        ResizeZone(int zone, int new_size);

    void        DeviceUpdateLEDs();
    void        UpdateZoneLEDs(int zone);
    void        UpdateSingleLED(int led);

    void        SetCustomMode();
    void        DeviceUpdateMode();

private:
    AuraGPUControllerv2* aura_gpu;

    int        GetDeviceMode();
};
