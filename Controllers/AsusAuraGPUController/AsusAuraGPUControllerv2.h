/*-----------------------------------------*\
|  AsusAuraGPUController.h                  |
|                                           |
|  Definitions for ASUS Aura RGB on GPUs    |
|                                           |
|  Jan Rettig (Klapstuhl) 14.02.2020        |
\*-----------------------------------------*/

#include <string>
#include "i2c_smbus.h"

#pragma once

typedef unsigned char aura_gpu_dev_id;

#define AURA_GPU_MAGIC_VAL 0x1589                   /* This magic value is present in all Aura GPU controllers */
#define AURA_GPU_APPLY_VAL 0x01                     /* Value for Apply Changes Register     */



class AuraGPUControllerv2
{
public:
    AuraGPUControllerv2(i2c_smbus_interface* bus, aura_gpu_dev_id);
    ~AuraGPUControllerv2();

    std::string   GetDeviceName();
    std::string   GetDeviceLocation();
    void          SetLEDColorsDirect(unsigned char red, unsigned char green, unsigned char blue, unsigned char * data);
    void          SetLEDColorsEffect(unsigned char red, unsigned char green, unsigned char blue, unsigned char * data);
    void          SetMode(unsigned char mode);

    unsigned char AuraGPURegisterRead(unsigned char reg, unsigned char len, unsigned char * vals);
    void          AuraGPURegisterWrite(unsigned char reg, unsigned char len, unsigned char * vals);

    bool          direct = false;                                                // Temporary solution to check if we are in "Direct" mode

private:
    char                    device_name[16];
    i2c_smbus_interface *   bus;
    aura_gpu_dev_id         dev;
};
